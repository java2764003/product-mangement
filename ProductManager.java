import java.util.ArrayList;
import java.util.Date;

class ProductManager {
    private ArrayList<Product> productList;

    public ProductManager() {
        this.productList = new ArrayList<>();
    }

    public void displayProducts() {
        if (productList.isEmpty()) {
            System.out.println("No products available.");
        } else {
            System.out.println("+-------+----------------------+------------+--------------+---------+--------------+");
            System.out.println("| ID    | Product Name         | Price      | Date Made    | Quality | Expires      |");
            System.out.println("+-------+----------------------+------------+--------------+---------+--------------+");
            for (Product product : productList) {
                System.out.println(product);
            }
            System.out.println("+-------+----------------------+------------+--------------+---------+--------------+");
        }
    }

    public void insertProduct(int productId, String productName, double productPrice, Date dateMake, int quality, Date expires) {
        Product product = new Product(productId, productName, productPrice, dateMake, quality, expires);
        productList.add(product);
        System.out.println("Product added successfully.");
    }

    public void searchProductByName(String productName) {
        for (Product product : productList) {
            if (product.getProductName().equalsIgnoreCase(productName)) {
                displayProductsHeader();
                System.out.println(product);
                displayProductsFooter();
                return;
            }
        }
        System.out.println("Product not found with name: " + productName);
    }

    public void searchProductById(int productId) {
        for (Product product : productList) {
            if (product.getProductId() == productId) {
                displayProductsHeader();
                System.out.println(product);
                displayProductsFooter();
                return;
            }
        }
        System.out.println("Product not found with ID: " + productId);
    }

    public void updateProductById(int productId, String newName, double newPrice, Date newDateMake, int newQuality, Date newExpires) {
        for (Product product : productList) {
            if (product.getProductId() == productId) {
                product.setProductName(newName);
                product.setProductPrice(newPrice);
                product.setDateMake(newDateMake);
                product.setQuality(newQuality);
                product.setExpires(newExpires);
                System.out.println("Product updated successfully.");
                return;
            }
        }
        System.out.println("Product not found with ID: " + productId);
    }

    private void displayProductsHeader() {
        System.out.println("+-------+----------------------+------------+--------------+---------+--------------+");
        System.out.println("| ID    | Product Name         | Price      | Date Made    | Quality | Expires      |");
        System.out.println("+-------+----------------------+------------+--------------+---------+--------------+");
    }

    private void displayProductsFooter() {
        System.out.println("+-------+----------------------+------------+--------------+---------+--------------+");
    }
}
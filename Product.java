import java.text.SimpleDateFormat;
import java.util.Date;

class Product {
    private int productId;
    private String productName;
    private double productPrice;
    private Date dateMake;
    private int quality;
    private Date expires;

    public Product(int productId, String productName, double productPrice, Date dateMake, int quality, Date expires) {
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.dateMake = dateMake;
        this.quality = quality;
        this.expires = expires;
    }

    public int getProductId() {
        return productId;
    }

    public String getProductName() {
        return productName;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public Date getDateMake() {
        return dateMake;
    }

    public int getQuality() {
        return quality;
    }

    public Date getExpires() {
        return expires;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public void setDateMake(Date dateMake) {
        this.dateMake = dateMake;
    }

    public void setQuality(int quality) {
        this.quality = quality;
    }

    public void setExpires(Date expires) {
        this.expires = expires;
    }

    @Override
    public String toString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return String.format("| %-5d | %-20s | %-10.2f | %-10s | %-7d | %-10s |",
                productId, productName, productPrice, dateFormat.format(dateMake), quality, dateFormat.format(expires));
    }
}

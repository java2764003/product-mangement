import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class ProductManagementApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ProductManager productManager = new ProductManager();

        while (true) {
            System.out.println("\nChoose the Option:");
            System.out.println("1. Display products");
            System.out.println("2. Insert product");
            System.out.println("3. Search product by name");
            System.out.println("4. Search product by ID");
            System.out.println("5. Update product by ID");
            System.out.println("6. Exit");

            int choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    productManager.displayProducts();
                    break;
                case 2:
                    System.out.println("Enter product ID:");
                    int productId = scanner.nextInt();
                    System.out.println("Enter product name:");
                    scanner.nextLine(); // Consume the newline character
                    String productName = scanner.nextLine();
                    System.out.println("Enter product price:");
                    double productPrice = scanner.nextDouble();
                    System.out.println("Enter Date Made (yyyy-MM-dd):");
                    scanner.nextLine(); // Consume the newline character
                    String dateMakeString = scanner.nextLine();
                    Date dateMake = parseDate(dateMakeString);

                    System.out.println("Enter product quality:");
                    int quality = scanner.nextInt();
                    System.out.println("Enter Expires (yyyy-MM-dd):");
                    scanner.nextLine(); // Consume the newline character
                    String expiresString = scanner.nextLine();
                    Date expires = parseDate(expiresString);

                    productManager.insertProduct(productId, productName, productPrice, dateMake, quality, expires);
                    break;
                case 3:
                    System.out.println("Enter product name to search:");
                    scanner.nextLine(); // Consume the newline character
                    String searchName = scanner.nextLine();
                    productManager.searchProductByName(searchName);
                    break;
                case 4:
                    System.out.println("Enter product ID to search:");
                    int searchId = scanner.nextInt();
                    productManager.searchProductById(searchId);
                    break;
                case 5:
                    System.out.println("Enter product ID to update:");
                    int updateId = scanner.nextInt();
                    System.out.println("Enter new product name:");
                    scanner.nextLine(); // Consume the newline character
                    String newName = scanner.nextLine();
                    System.out.println("Enter new product price:");
                    double newPrice = scanner.nextDouble();
                    System.out.println("Enter new Date Made (yyyy-MM-dd):");
                    scanner.nextLine(); // Consume the newline character
                    String newDateMakeString = scanner.nextLine();
                    Date newDateMake = parseDate(newDateMakeString);

                    System.out.println("Enter new product quality:");
                    int newQuality = scanner.nextInt();
                    System.out.println("Enter new Expires (yyyy-MM-dd):");
                    scanner.nextLine(); // Consume the newline character
                    String newExpiresString = scanner.nextLine();
                    Date newExpires = parseDate(newExpiresString);

                    productManager.updateProductById(updateId, newName, newPrice, newDateMake, newQuality, newExpires);
                    break;
                case 6:
                    System.out.println("Exiting the program. Goodbye!");
                    System.exit(0);
                default:
                    System.out.println("Invalid option. Please choose a valid option.");
            }
        }
    }

    private static Date parseDate(String dateString) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            return dateFormat.parse(dateString);
        } catch (Exception e) {
            System.out.println("Error parsing date. Using current date.");
            return new Date();
        }
    }
}